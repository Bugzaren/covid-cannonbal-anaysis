
def SaveNumpyAsHDF5(NumpyTensor,FullFileName,DataSetName):
    import os
    import h5py
    SaveDir=os.path.dirname(FullFileName)
    if not os.path.exists(SaveDir):
        os.makedirs(SaveDir)
    try:
        print(("Open file '"+FullFileName+"' for writing..."))
        of = h5py.File(FullFileName, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + FullFileName  + ': ' + str(ioerror)))
        raise IOError
    of.create_dataset(DataSetName, data=NumpyTensor)
    of.close()
    print("Saved",FullFileName)

def SaveManyNumpyAsHDF5(NumpyTensorList,FullFileName,DataSetNameList):
    import os
    import h5py
    Tlen=len(NumpyTensorList)
    Dlen=len(DataSetNameList)
    if Tlen!=Dlen:
        error("The list of Numpy tensors and the list of data set names have differnt lengths")
    SaveDir=os.path.dirname(FullFileName)
    if not os.path.exists(SaveDir):
        os.makedirs(SaveDir)
    try:
        print(("Open file '"+FullFileName+"' for writing..."))
        of = h5py.File(FullFileName, 'w')
    except IOError as ioerror:
        print(("ERROR: Problem writing to file " + FullFileName  + ': ' + str(ioerror)))
        raise IOError
    for indx in range(Tlen):
        of.create_dataset(DataSetNameList[indx],
                          data=NumpyTensorList[indx])
    of.close()
    print("Saved",FullFileName)

    
def LoadNumpyfromHDF5(FullFileName,Header):
    import os
    import h5py
    import numpy as np
    print(("Open file '"+FullFileName+"' for reading..."))
    with h5py.File(FullFileName, 'r') as filepointer:
        NpData=np.array(filepointer.get(Header))
    print(("Data read from file '"+FullFileName))
    return NpData

def LoadManyNumpyfromHDF5(FullFileName,HeaderList):
    import os
    import h5py
    import numpy as np
    print(("Open file '"+FullFileName+"' for reading..."))
    NpDataList=[]
    with h5py.File(FullFileName, 'r') as filepointer:
        for Header in HeaderList:
            NpDataList.append(np.array(filepointer.get(Header)))
            print(("Data '"+Header+"' read from file '"+FullFileName+"'"))
    return NpDataList

def savefig(FullFileName):
    import matplotlib.pyplot as plt
    import os
    SaveDir=os.path.dirname(FullFileName)
    if not os.path.exists(SaveDir):
        os.makedirs(SaveDir)
    print("Saving to: "+FullFileName+".png/pdf")
    plt.savefig(FullFileName+".png")
    plt.savefig(FullFileName+".pdf")

    
