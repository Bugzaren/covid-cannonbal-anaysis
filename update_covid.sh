#!/bin/bash

set -e -u

MYPATH=$(pwd)
echo MYPATH: $MYPATH
cd ../2020-covid-19-data
git pull

cd "$MYPATH"

python3 covid-plotting.py -q

####Clean the plot catalouge
rm -f countries/*


python3 all_countries.py -q

###Make a special copy for the world
cp countries/World_deaths_202*.png countries/World_deaths.png

echo start > log.txt

####Make the html files
python3 make_html.py >> log.txt

#TargertURL=thomas:www_fremling
TargertURL=uu_gemini:www

rsync --progress --delete -r countries ${TargertURL}/corona
rsync --progress -r html ${TargertURL}/corona
rsync --progress -r index.html ${TargertURL}/corona
rsync --progress -r Prognosis.hdf5 ${TargertURL}/corona
rsync --progress -r pic ${TargertURL}/corona

