def MkLink(Text,URL):
    return "<a href=\""+URL+"\">"+Text+"</a>"

import numpy as np
import pandas as pd
import sys
import argparse
from NumpyHDF5 import *


parser = argparse.ArgumentParser(description='Analize the covid data')
parser.add_argument('-q', action="store_true",
                    help='Be quiet == Do not show plots')
args = parser.parse_args()


CountryList,CasesPrognosis,DeathsPrognosis,DateListS=LoadManyNumpyfromHDF5(
    "Prognosis.hdf5",
    ["CountryList","CasesPrognosis","DeathsPrognosis","DateListS"])

CountryList =  np.array(CountryList,dtype='U')  ###Make Unicode
 


TheContintents=np.array(['World','Africa', 'Asia', 'Europe', 'North America',
                           'Oceania','South America'],dtype=str)
##TheContintents=np.array([]) ####Skip the continents

Countries=np.setdiff1d(CountryList,TheContintents)###Remove the conintnets
PresentationList=np.append(TheContintents,Countries)
print(PresentationList)
#PresentationList=["France"]


###The headers:
##print("The headers: ",Data.columns)

MainFile="index.html"

FileObject  = open("html/"+MainFile, "w") 

FileObject.write("<html>\n")
FileObject.write("<head>\n")
FileObject.write("   <title>Corona Cannonball</title>\n")
FileObject.write('   <meta property="og:title" content="Corona Cannonball"\n')
FileObject.write('   <meta property="og:type" content="article"\n')
FileObject.write('   <meta property="og:url" content="https://webspace.science.uu.nl/~freml001/corona"\n')
FileObject.write('   <meta property="og:image" content="https://webspace.science.uu.nl/~freml001/corona/pic/deaths_canonbal_linear.png"\n')
FileObject.write('   <meta property="og:description" content="The home of the corona tracker using cannonball diagrams"\n')
FileObject.write("</head>\n")

FileObject.write("<body>\n")
FileObject.write("<h2>Corona Cannonball</h2>\n")

FileObject.write("As every armchair epidemiologist, i keep my own statistics of the corona cases for all countries in the world. I find myself referring to them quite often when talking to people, so I thought it was about time to make them accessible through thus webpage: <br><br>\n")

FileObject.write("What do these offer that you cannot find anywhere else?<br>\n")

FileObject.write("These plots offer a somewhat unconventional view of the corona cases/deaths in the form of a Canonball diagram (Left panel). On the vertical axis you will find the new cases per day, and on the horizontal you will find the total number of cases. Thus time is implicit in these plots.<br><br>\n")

FileObject.write("Why is this useful?<br>\n")

FileObject.write("If you have processes governed by exponential growth or decline, these will show up as straight lines -- which are easy to identify.<br>\n")

FileObject.write("These straight lines can then be used to make predictions for the future development, and in the case of decline can make a decent prediction for where the total numbers will end up. The projected development over the next two months is the visualized in the right hand panel. <br><br>\n")

FileObject.write("Hope you find these graphs as useful as i have.<br><br>\n")


FileObject.write(MkLink("Mikael Fremling",
                        "https://webspace.science.uu.nl/~freml001")
                 +" - micke.fremling@gmail.com<br><br>\n")

FileObject.write("PS. Come back tomorrow for the daily updates. DS<br><br>\n")

FileObject.write("First some comparisons between select countries.<br><br>\n")
FileObject.write(MkLink("<img width=\"800\" src=\"../pic/cases_canonbal_linear.png\" alt=\""+
                        "Cases coparison\">",
                        "../pic/cases_canonbal_linear.png")+"\n")
FileObject.write(MkLink("<img width=\"800\" src=\"../pic/deaths_canonbal_linear.png\" alt=\""+
                        "Cases coparison\">",
                        "../pic/deaths_canonbal_linear.png")+"<br>\n")



def RateToStr(Rate,Type):
    if len(Rate) >= 1:
        Rate = Rate[0]
    print("Rate:",Rate)
    print("Type:",Type)
    ProjectionRate=(Rate-1)*100
    if np.isnan(Rate):
        ProjStr = ""
    else:
        ProjStr="%2.1f %%" % ProjectionRate
        ProjStr+=" "+Type+" per day"
    Col=RateToCol(ProjectionRate)
    ProjStr = '<span style="background-color: '+Col+'">'+ProjStr+"</span>"
    return ProjStr


def RateToCol(Rate):
    CapValue=8.0
    if np.isnan(Rate):
        return "#ffffff"
    else:
        AbsRate=np.abs(Rate)
        print("AbsRate:",AbsRate)
        RelRate=np.min([CapValue,AbsRate])/CapValue
        print("RelRate:",RelRate)
        BinVal=np.floor(255*(1-RelRate))
        print("BinVal:",BinVal)
        Hex=hex(int(BinVal))[2:]
        print("Hex:",Hex)
        if len(Hex) == 1:
            Hex += "0"
            print("Hex - padded:",Hex)
       
        if Rate >= 0:
            return "#ff"+Hex+Hex
        else: #Rate < 0
            BinVal=np.floor(255*(1-np.min([CapValue,-Rate])/CapValue))
            return "#"+Hex+"ff"+Hex
    


for Country in PresentationList:
    print("Country:", Country)
    DeathProg = DeathsPrognosis[CountryList == Country]
    CasesProg = CasesPrognosis[CountryList == Country]
    DeathStr=RateToStr(DeathProg,"deaths")
    CasesStr=RateToStr(CasesProg,"cases")
    DateS = DateListS[CountryList == Country][0]
    Date = str(DateS)[2:-1]
    
    if Country == "Afghanistan":
        FileObject.write("<hr><h2>Countries</h2>\n")
    if Country == "Africa":
        FileObject.write("<hr><h2>Continents</h2>\n")
    CountryFile  = Country+".html"
    CountryFileObject  = open("html/"+CountryFile, "w")
    if Country == "World":
        FileObject.write("<h2>"+MkLink(Country,CountryFile)+"</h2><h3>"+CasesStr+", "+DeathStr+"</h3>\n")
    elif np.sum([Country==TheContintents])==1:
        FileObject.write("<h3>"+MkLink(Country,CountryFile)+"</h3><h3>"+CasesStr+", "+DeathStr+"</h3>\n")
    else:
        FileObject.write("<h3>"+MkLink(Country,CountryFile)+" "+CasesStr+", "+DeathStr+"</h3>\n")
    CountryFileObject.write("<html>\n")
    CountryFileObject.write("<head>\n")
    LocalTitle="Corona Cannonball - "+Country
    CountryFileObject.write("<title>"+LocalTitle+"</title>\n")
    CountryFileObject.write('   <meta property="og:title" content="'+LocalTitle+'"\n')
    CountryFileObject.write('   <meta property="og:type" content="article"\n')
    CountryFileObject.write('   <meta property="og:url" content="https://webspace.science.uu.nl/~freml001/corona/html/'+CountryFile+'"\n')
    CountryFileObject.write('   <meta property="og:image" content="https://webspace.science.uu.nl/~freml001/corona/countries/'+Country+'_deaths_short_'+Date+'.png"\n')
    CountryFileObject.write('   <meta property="og:description" content="The latest cannonball diagrams for '+Country+'."\n')

    CountryFileObject.write("</head>\n")
    CountryFileObject.write("<body>\n")

    
    CountryFileObject.write("Back to "+MkLink("main",MainFile)+"<br>\n")
    CountryFileObject.write("<h1>Corona Cannonball - "+Country+"</h1><br>")
    for Type in ["deaths","cases","hosp","icu"]:
        CountryFileObject.write("<h2>"+Type+"</h2>\n")
        for ShortTerm in [False,True]:
            if ShortTerm:
                SortStr="_short"
                TypeName="recent "+Type
            else:
                SortStr=""
                TypeName="all "+Type
            PNGfile="countries/"+Country+"_"+Type+SortStr+"_"+Date+".png"
            PDFfile="countries/"+Country+"_"+Type+SortStr+"_"+Date+".pdf"
            #FileObject.write(MkLink(TypeName,"../"+PNGfile)+" ")
            CountryFileObject.write(MkLink("<img src=\"../"+PNGfile+"\" alt=\""+
                                           TypeName+" in "+Country+"\">",
                                           "../"+PNGfile)+"<br>\n")
        #FileObject.write("<br>\n")
    CountryFileObject.write("Back to " +MkLink("main", MainFile)+"<br>\n")
    #FileObject.write("<br>\n")
    CountryFileObject.write("</body>\n")
    CountryFileObject.write("</html>\n")
    

FileObject.write("<br>\n")
FileObject.write("This website uses open source data from "+
                 MkLink("Our World in Data","https://ourworldindata.org/")+
                 " available throug "+
                 MkLink("this link","https://github.com/owid/covid-19-data/tree/master/public/data")
                 +".<br>\n")

FileObject.write("</body>\n")
FileObject.write("</html>\n")
