import numpy as np
import pandas as pd
import sys
import argparse

parser = argparse.ArgumentParser(description='Analize the covid data')
parser.add_argument('-q', action="store_true",
                    help='Be quiet == Do not show plots')
args = parser.parse_args()


if args.q:
    import matplotlib
    matplotlib.use('Agg')
import matplotlib.pyplot as plt 



Data = pd.read_table("../2020-covid-19-data/public/data/owid-covid-data.csv",
                     delimiter=",")


###The headers:
print("The headers: ",Data.columns)

CountryList=["Sweden","Netherlands","Brazil","United States","Japan","South Africa","Russia","India","United Kingdom"]#,"South Korea"]
#CountryList=["Netherlands","United Kingdom"]#,"South Korea"]
#CountryList=["South Korea","China"]


UniqueDates = np.unique(Data.loc[:,'date'])
FirstInMonth=UniqueDates[np.array([x[8:10]=="01" or
                                   x[8:10]=="11" or
                                   x[8:10]=="21" 
                                   for x in UniqueDates])]


#Type="cases"
#Type="deaths"
for Type in ["deaths","cases"]:

    XMAX=0
    FitHist=14
    AggLen=7
    plt.figure(figsize=(10,7))
    ymax = 0
    for Country in CountryList:
        print("Country:", Country)
        LocalData=Data.loc[Data.loc[:,"location"]==Country]
        CasePerM=np.array(LocalData.loc[:,'total_'+Type+'_per_million'])
        DailyCasePerM=np.array(LocalData.loc[:,'new_'+Type+'_per_million'])
        WeeklyCasePerM=np.array(DailyCasePerM[:-AggLen])
        CasePerMW=np.array(CasePerM[:-AggLen])
        for day in range(1,AggLen):
            WeeklyCasePerM = WeeklyCasePerM + DailyCasePerM[day:-(AggLen-day)]
            CasePerMW = CasePerMW +CasePerM[day:-(AggLen-day)]
        ###Set zeros to np.nan
        WeeklyCasePerM/=AggLen
        CasePerMW/=AggLen

        TOFFirstInMonth=np.array([x[8:10]=="01" for x in LocalData.loc[:,'date']])

        coef,Cov1 = np.polyfit(CasePerMW[-FitHist:],WeeklyCasePerM[-FitHist:],
                               1,cov=True)
        poly1d_fn = np.poly1d(coef)

        
        # poly1d_fn is now a function which takes in x and returns an estimate for y


        ###We know want to solver the equation
        ##  y^2 = Err1, where y=a1 + a0*x (yes its backwards)
        ## we thus want to solve
        ## x^2 a0^2 + 2*a0*a1*x + a1**2 = c11 + (c10 + c01)*x + c00 x**2
        ###This gives
        ###   x^2 + p*x + q = 0, '
        ###  where q=(a1^2-c11)/(a0^2-c00)
        ### and    p = ( 2*a0*a1 - c10 - c01)/(a0^2-c00)
        ##with solution
        ###  x0 =  (p/2) +- sqrt(q + (p^2)/4)
        q = ( coef[1]**2 - Cov1[1,1]) / ( coef[0]**2 - Cov1[0,0] )
        p = ( 2*coef[1]*coef[0] - Cov1[0,1] - Cov1[0,1] ) / ( coef[0]**2 - Cov1[0,0] )
        print("p:",p)
        print("q:",q)

        SQTERM = np.sqrt( -q + 0.25*p**2)
        XMID= -0.5*p
        print("XMID:",XMID)
        print("SQTERM:",SQTERM)
        print("x-:",XMID - SQTERM)
        print("x+:",XMID + SQTERM)

        if coef[0]<=0:
            Intercept=-coef[1]/coef[0]
            if XMID + SQTERM > Intercept:
                TheRange=np.linspace(CasePerMW[-FitHist],XMID + SQTERM)
                Label=(str(int(np.round(XMID - SQTERM)))+
                       "~"+str(int(np.round(Intercept)))+
                       "~"+str(int(np.round(XMID + SQTERM))))
            else:
                TheRange=np.linspace(CasePerMW[-FitHist],Intercept)
                Label=(str(int(np.round(XMID - SQTERM)))+
                       "~"+str(int(np.round(Intercept)))+
                       "~$\infty$")

        else:
            DiffCases=CasePerMW[-1]-CasePerMW[-FitHist]
            if XMID + SQTERM > CasePerMW[-1]:
                TheRange=np.linspace(CasePerMW[-FitHist],XMID + SQTERM)
                Label=(str(int(np.round(XMID + SQTERM)))+
                       "~$\infty$")
            else:
                TheRange=np.linspace(CasePerMW[-FitHist],CasePerMW[-1]+DiffCases)
                Label=""



        #Err1=TheRange*0.0
        #for a in range(2):
        #    for b in range(2):
        #        Err1+=Cov1[1-a,1-b]*TheRange**(a+b)
        ###
        Err1=Cov1[1,1]+(Cov1[1,0]+Cov1[0,1])*TheRange+Cov1[0,0]*TheRange**2
        Err1=np.sqrt(Err1)

            
        #line=plt.plot(CasePerM,DailyCasePerM,"-",label=Country)
        line=plt.plot(CasePerMW,WeeklyCasePerM,"-p",label=Country+" "+Label)

        ymax=np.max([ymax,np.nanmax(WeeklyCasePerM)])
        
        col=line[0].get_color()
        print("XMAX:",XMAX)
        print("np.nanmax(CasePerMW):",np.nanmax(CasePerMW))
        XMAX=np.max([np.nanmax(CasePerMW)*1.1,XMAX])
        print("XMAX:",XMAX)
        plt.plot(TheRange, poly1d_fn(TheRange), '--',color=col)
        plt.plot(TheRange, poly1d_fn(TheRange)+Err1,':',color=col,
                 label="__no_label")
        plt.plot(TheRange, poly1d_fn(TheRange)-Err1,':',color=col,
                 label="__no_label")

        plt.xlim([0,XMAX])
        #plt.show(block=False)
        #raw_input("wait")
        
    plt.title("Number of "+Type+" in a cannonball diagram")
    plt.ylim([0,ymax*1.1])
    plt.xlim([0,XMAX])
    plt.xlabel("Number of total "+Type+" per Million")
    plt.ylabel("Number of new "+Type+" per day (aggregaded per week) per M")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("pic/"+Type+"_canonbal_linear.png")
    plt.savefig("pic/"+Type+"_canonbal_linear.pdf")


    #raw_input("Press enter")
    #sys.exit()
    
    plt.figure()
    for Country in CountryList:
        print("Country:", Country)
        LocalData=Data.loc[Data.loc[:,"location"]==Country]

        CasePerM=LocalData.loc[:,'total_'+Type+'_per_million']
        Dates = LocalData.loc[:,'date']

        plt.plot(Dates,CasePerM,"-",label=Country)
    plt.xticks(FirstInMonth,FirstInMonth,rotation="vertical")
    plt.xlim(["2020-03-10",plt.xlim()[1]])
    plt.title(Type+" per million people")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("pic/"+Type+"_total.png")
    plt.savefig("pic/"+Type+"_total.pdf")


    
    AggLen=7
    Before=AggLen//2
    plt.figure()
    for Country in CountryList:
        print("Country:", Country)
        LocalData=Data.loc[Data.loc[:,"location"]==Country]
        Dates = LocalData.loc[:,'date']
        CasePerM=np.array(LocalData.loc[:,'total_'+Type+'_per_million'])
        DailyCasePerM=np.array(LocalData.loc[:,'new_'+Type+'_per_million'])
        WeeklyCasePerM=np.array(DailyCasePerM[:-AggLen])
        for day in range(1,AggLen):
            WeeklyCasePerM = WeeklyCasePerM + DailyCasePerM[day:-(AggLen-day)]
        ###Set zeros to np.nan
        WeeklyCasePerM/=AggLen

        #line=plt.plot(CasePerM,DailyCasePerM,"-",label=Country)
        line=plt.plot(Dates[Before:-(Before+1)],WeeklyCasePerM,"-p",label=Country)
        #plt.show(block=False)
        #raw_input("wait")

    plt.xticks(FirstInMonth,FirstInMonth,rotation="vertical")
    plt.xlim(["2020-03-10",plt.xlim()[1]])
    plt.title(Type+" per day (average over  a week)")
    plt.ylim([0,plt.ylim()[1]])
    plt.xlabel("Date")
    plt.ylabel("Number of new "+Type+" per day\\(aggregaded per week) per M")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("pic/"+Type+"_per_week.png")
    plt.savefig("pic/"+Type+"_per_week.pdf")

    AggLen=7
    plt.figure()
    plt.loglog()
    for Country in CountryList:
        print("Country:", Country)
        LocalData=Data.loc[Data.loc[:,"location"]==Country]
        CasePerM=np.array(LocalData.loc[:,'total_'+Type+'_per_million'])
        DailyCasePerM=np.array(LocalData.loc[:,'new_'+Type+'_per_million'])
        WeeklyCasePerM=np.array(DailyCasePerM[:-AggLen])
        CasePerMW=np.array(CasePerM[:-AggLen])
        for day in range(1,AggLen):
            WeeklyCasePerM += DailyCasePerM[day:-(AggLen-day)]
            CasePerMW += CasePerM[day:-(AggLen-day)]
        ###Set zeros to np.nan
        WeeklyCasePerM=WeeklyCasePerM/AggLen
        CasePerMW=CasePerMW/AggLen

        TOFFirstInMonth=np.array([x[8:10]=="01" for x in LocalData.loc[:,'date']])


        #line=plt.plot(CasePerM,DailyCasePerM,"-",label=Country)
        line=plt.plot(CasePerMW,WeeklyCasePerM,"-",label=Country)
        col=line[0].get_color()
        plt.plot(CasePerMW[TOFFirstInMonth[:-AggLen]],
                 WeeklyCasePerM[TOFFirstInMonth[:-AggLen]],
                 "p",label="__nolabel",color=col)
    plt.title(Type+": Cannonball diagram (log-log)")
    plt.xlabel("Number of total "+Type+" per Million")
    plt.ylabel("Number of new "+Type+" per day (aggregaded per week) per M")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("pic/"+Type+"_canonbal_log.png")
    plt.savefig("pic/"+Type+"_canonbal_log.pdf")




    AggLen=7
    plt.figure()
    for Country in CountryList:
        print("Country:", Country)
        LocalData=Data.loc[Data.loc[:,"location"]==Country]
        Dates = LocalData.loc[:,'date']
        CasePerM=np.array(LocalData.loc[:,'total_'+Type+'_per_million'])
        DailyCasePerM=np.array(LocalData.loc[:,'new_'+Type+'_per_million'])
        WeeklyCasePerM=np.array(DailyCasePerM[:-AggLen])
        CasePerMW=np.array(CasePerM[:-AggLen])

        for day in range(1,AggLen):
            WeeklyCasePerM += DailyCasePerM[day:-(AggLen-day)]
            CasePerMW += CasePerM[day:-(AggLen-day)]
        ###Set zeros to np.nan
        WeeklyCasePerM/=AggLen
        CasePerMW/=AggLen

        TOFFirstInMonth=np.array([x[8:10]=="01" for x in LocalData.loc[:,'date']])


        plt.plot(Dates[:-AggLen],100*WeeklyCasePerM/CasePerMW,"-",label=Country)
    plt.title("Grows of "+Type+" per day (in %)")
    plt.xticks(FirstInMonth,FirstInMonth,rotation="vertical")
    plt.xlim(["2020-03-01",plt.xlim()[1]])
    plt.ylim([0,20])
    plt.legend(loc="best")
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("pic/"+Type+"_increase_rate.png")
    plt.savefig("pic/"+Type+"_increase_rate.pdf")



if not args.q:
    input("Press enter")

    
