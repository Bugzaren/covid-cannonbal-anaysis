
def ComputeIntercept(coef,Cov1,FitStart,FitStop):
    ###We know want to solver the equation
    ##  y^2 = Err1, where y=a1 + a0*x (yes its backwards)
    ## we thus want to solve
    ## x^2 a0^2 + 2*a0*a1*x + a1**2 = c11 + (c10 + c01)*x + c00 x**2
    ###This gives
    ###   x^2 + p*x + q = 0, '
    ###  where q=(a1^2-c11)/(a0^2-c00)
    ### and    p = ( 2*a0*a1 - c10 - c01)/(a0^2-c00)
    ##with solution
    ###  x0 =  (p/2) +- sqrt(q + (p^2)/4)
    q = ( coef[1]**2 - Cov1[1,1]) / ( coef[0]**2 - Cov1[0,0] )
    p = ( 2*coef[1]*coef[0] - Cov1[0,1] - Cov1[0,1] ) / ( coef[0]**2 - Cov1[0,0] )
    print("p:",p)
    print("q:",q)

    SQTERM = np.sqrt( -q + 0.25*p**2)
    XMID= -0.5*p
    #print "XMID:",XMID
    #print "SQTERM:",SQTERM
    #print "x-:",XMID - SQTERM
    #print "x+:",XMID + SQTERM

    if coef[0]<=0:
        print("Negative slope")
        Intercept=-coef[1]/coef[0]
        print("Intercept:", Intercept)
        if XMID + SQTERM > Intercept:
            TheRange=np.linspace(FitStart,XMID + SQTERM)
            Label=(str(int(np.round(XMID - SQTERM)))+
                   "~"+str(int(np.round(Intercept)))+
                   "~"+str(int(np.round(XMID + SQTERM))))
        else:
            TheRange=np.linspace(FitStart,Intercept)
            Label=(str(int(np.round(XMID - SQTERM)))+
                   "~"+str(int(np.round(Intercept)))+
                   "~$\infty$")

    else:
        print("Positive slope cases")
        DiffCases=FitStop-FitStart
        if XMID + SQTERM > FitStop:
            TheRange=np.linspace(FitStart,XMID + SQTERM)
            Label=(str(int(np.round(XMID + SQTERM)))+
                   "~$\infty$")
        else:
            TheRange=np.linspace(FitStart,FitStop+DiffCases)
            Label=""
        Intercept=None  
    return Intercept, TheRange, Label



def GetProjection(coef,Cov1,N0,TimeForward,Plus=False):
    ###Starting point
    Projection=np.zeros(TimeForward)
    Projection2=np.zeros(TimeForward*2)
    NewProjection=np.zeros(TimeForward)
    NewProjection2=np.zeros(TimeForward*2)
    #print "N0 start",N0
    for dayno in range(TimeForward):
        N1=NextDayNumbers(coef[0],coef[1],Cov1[1,1],Cov1[0,1],Cov1[0,0],N0,Plus=Plus)
        #print "day no:",dayno
        #print "N0:",N0
        #print "N1:",N1
        #print "N1-N0:",N1-N0
        Projection[dayno]=N1
        NewProjection[dayno]=N1-N0
        Projection2[2*dayno]=N0
        Projection2[2*dayno+1]=N1
        NewProjection2[2*dayno]=0
        NewProjection2[2*dayno+1]=N1-N0
        N0=N1
    return Projection,NewProjection


def NextDayNumbers(k,m,a,b,c,N0,Plus=True):
    if Plus:
        Sign = 1
    else:
        Sign = -1

    alpha=1.-k
    beta=N0+m
    #a=0
    #b=0
    #c=0
    p=(alpha*beta + b) / (alpha**2 - c)
    q=(a - beta**2) / (alpha**2 - c)
    x=p + Sign*np.sqrt(np.abs(q + p**2))
    #print "alpha:",alpha
    #print "beta:",beta
    #print "p:",p
    #print "q:",q
    #print "q + p**2:",q + p**2
    #print "x:",x


    if False:
        NL=np.arange(N0,x+1)
        plt.figure()
        plt.plot(NL,NL*k+m)
        plt.plot(NL,NL*k+m+np.sqrt(a+2*b*NL+c*NL**2))
        plt.plot(NL,NL*k+m-np.sqrt(a+2*b*NL+c*NL**2))
        plt.plot([N0,x],[0,x-N0],"--p")

        plt.show(block=False)
        input("Wait")
    #return  (N0 +m + 0.0)/(1-k)
    return x

    
def MakeFitAndIntercept(TotalCases,NewCases):
    ClearNaN=np.logical_not(np.isnan(TotalCases) + np.isnan(NewCases))
    try:
        coef,Cov1 = np.polyfit(TotalCases[ClearNaN],
                               NewCases[ClearNaN],
                               1,cov=True)
        print("coeff:",coef)
        Lambda=coef[0]*1.0
        Aval=-coef[1]/Lambda
        print("Lambda:",Lambda)
        print("Aval:",Aval)
        ProjectionWorked=True
    except:
        Lambda=0.0
        Aval=0.0
        Intercept=0.0
        coef = None
        Cov1 = None
        ProjectionWorked=False

    if np.isnan(Lambda):
        ProjectionWorked=False

    if np.all(Cov1==0.0):
        ProjectionWorked=False
        Cov1=None
        Coef=None

    if ProjectionWorked and np.isclose(coef[0],1): ##If the slope is close to one, the growth is infinite....
        ProjectionWorked=False


    #print "coef",coef
    #print "Cov1",Cov1

    # poly1d_fn is now a function which takes in x and returns an estimate for y

    if ProjectionWorked:
        Intercept, TheRange, Label=ComputeIntercept(coef,Cov1,TotalCases[0],TotalCases[-1])

        #Err1=TheRange*0.0
        #for a in range(2):
        #    for b in range(2):
        #        Err1+=Cov1[1-a,1-b]*TheRange**(a+b)
        ###
        Err1=Cov1[1,1]+(Cov1[1,0]+Cov1[0,1])*TheRange+Cov1[0,0]*TheRange**2
        Err1=np.sqrt(Err1)
    else:
        Label=""
        Err1=None
        TheRange=None
        Intercept=None
    return coef, Cov1, Intercept, Err1, TheRange, Label, ProjectionWorked


def give_ylim(Cases,Verbose=False):
    return give_xlim(Cases,None,Verbose=Verbose)

def give_xlim(Cases,Intercept,Verbose=False):
    #print("Cases:",Cases)
    if len(Cases) == 0:
        return 0,1
    X0MAX=np.nanmax(Cases)
    X0MIN=np.nanmin(Cases)
    X0MIN=np.max([0,X0MIN])
    if Verbose:
        print("X0MIN:",X0MIN)
        print("X0MAX:",X0MAX)
    if X0MIN==0:
        XMIN=0
    else:
        if X0MAX-(X0MAX-X0MIN)*1.5 < 0:
            XMIN = 0
        else: ##If the zero can be seen, extend the axis longer, otherwise done
            XMIN=max(0,X0MAX-(X0MAX-X0MIN)*1.1)

    if Intercept ==None:
        XMAX=X0MIN+(X0MAX-X0MIN)*1.1
    else:
        if Intercept < X0MAX+(X0MAX-X0MIN):
            XMAX = np.max([X0MAX+(Intercept-X0MAX)*1.3,
                          X0MIN+(X0MAX-X0MIN)*1.1])
        else:
            XMAX=X0MIN+(X0MAX-X0MIN)*1.1

    if np.isnan(XMAX):
        XMAX=1
        XMIN=0

    ###Uefull later
    ###    Y1=[np.max([0,-(Y1[1]-Y1[0])+Y1[0]]), np.min([Y1NEW[1],2*(Y1[1]-Y1[0])+Y1[0]])]
    ###Y2=[np.max([0,-(Y2[1]-Y2[0])+Y2[0]]), np.min([Y2NEW[1],2*(Y2[1]-Y2[0])+Y2[0]])]        
    return XMIN,XMAX
        

def MakeWeeklyAverage(Cases):
    CasesWeekly=np.array(Cases[(AggLen-1):])
    for day in range(1,AggLen):
        CasesWeekly = np.nansum([CasesWeekly,Cases[(AggLen-1-day):-day]],axis=0)
    CasesWeekly=CasesWeekly/(1.0*AggLen)
    return CasesWeekly
    

def bf(Txt):
    return "$\\bf{"+Txt+"}$"

def TypeToStr(Type):
    if Type == "deaths":
        return 'new_'+Type
    if Type == "cases":
        return 'new_'+Type
    if Type == "icu":
        return 'weekly_'+Type+'_admissions'
    if Type == "hosp":
        return 'weekly_'+Type+'_admissions'

def StretchWeek(A):
    #print("A:",A)
    if len(A) == 0:
        return np.array([])
    return np.append(np.append([A[0],A[0],A[0]],1.0 * A),[A[-1],A[-1],A[-1]])

def MakeCountryPlot(LocalData,Type,ShortTerm,Country):
    plt.figure(figsize=(15,6))
    print("Country:", Country)

    LongTypeStr=TypeToStr(Type)
    
    NewCasesDaily=np.array(LocalData.loc[:,LongTypeStr])#+'_per_million'])
    #TotCasesDaily=np.array(LocalData.loc[:,'total_'+Type])#+'_per_million'])
    TotCasesDaily=np.nancumsum(NewCasesDaily)
    #print("TotCasesDaily:",TotCasesDaily)
    NewCasesWeekly=MakeWeeklyAverage(NewCasesDaily)
    TotCasesWeekly=MakeWeeklyAverage(TotCasesDaily)
    if Type == "icu" or Type == "hosp":
        NewCasesDaily = StretchWeek(NewCasesWeekly)
        TotCasesDaily = StretchWeek(TotCasesWeekly)
        
    Dates = LocalData.loc[:,'date']
    BaseDate = Dates.iloc[0]
    #print("Dates:", Dates)
    NumOfDates=len(Dates)
    print("NumOfDates:", NumOfDates)
    DatesNum=np.array([(datetime.strptime(y, "%Y-%m-%d") -
                         datetime.strptime(BaseDate, "%Y-%m-%d")).days
                        for y in Dates])
    coef,Cov1,Intercept, Err1, TheRange, Label, ProjectionWorked = (
        MakeFitAndIntercept(TotCasesWeekly[-FitHist:],NewCasesWeekly[-FitHist:]))
    poly1d_fn = np.poly1d(coef)

    #line=plt.plot(TotCasesDaily,NewCasesDaily,"-",label=Country)

    if Type=="deaths":
        colNewDay="cyan"
        colNewWeek="blue"
        colTotDay="#fcba03"
        colTotWeek="#b5610d"
        colFit="green"
    elif Type=="cases":
        colNewDay="#ea98af"
        colNewWeek="red"
        colTotDay="#cd77e0"
        colTotWeek="#960db5"
        colFit="orange"
    else:
        colNewDay="#86f268"
        colNewWeek="#20db46"
        colTotDay="#dba320"
        colTotWeek="#db4620"
        colFit="#b6db20"

    if ShortTerm:
        TimeForward=30
        DFLIST=[30]
    else:
        TimeForward=60
        DFLIST=[30,60]

    print("TimeFor:", TimeForward)


        
    if ProjectionWorked:
        TotProjection,NewProjection = GetProjection(coef,0.0*Cov1,TotCasesWeekly[-1],
                                   TimeForward)
        TotProjectionP,NewProjectionP = GetProjection(coef,Cov1,TotCasesWeekly[-1],
                                    TimeForward,Plus=True)
        TotProjectionM,NewProjectionM = GetProjection(coef,Cov1,TotCasesWeekly[-1],
                                    TimeForward,Plus=False)
        print("length projektion:", len(TotProjection))

        

    if ProjectionWorked:
        print("Old step:",NewProjection[0])
        print("New step:",NewProjection[1])
        IncreaseRate=NewProjection[1]/(1.0*NewProjection[0])
        ProjectionRate=(IncreaseRate-1)*100
        if ProjectionRate > 0:
            ProjStr=": %2.1f %% increase per day" % ProjectionRate
        else:
            ProjStr=": %2.1f %% decrease per day" % ProjectionRate
    else:
        ProjStr=""
        
    plt.subplot(1,2,1)    
    plt.plot(TotCasesDaily,NewCasesDaily,"--p",color=colNewDay,label=bf("daily")+" "+bf(Type)+ProjStr)

    plt.plot(TotCasesWeekly,NewCasesWeekly,"-p",color=colNewWeek,label=bf("daily")+" "+bf(Type)+": average per 7 days")
    if ShortTerm:
        XLIM=give_xlim(TotCasesDaily[-ShortHistory:],Intercept)
        YLIM=give_ylim(NewCasesDaily[-ShortHistory:])
        SortStr="_short"
    else:
        XLIM=give_xlim(TotCasesDaily,Intercept)
        YLIM=give_ylim(NewCasesDaily)
        SortStr=""

    if ProjectionWorked:
        plt.plot(TheRange, poly1d_fn(TheRange), '--',color=colFit,
                 label="Projection "+Label)
        plt.plot(TheRange, poly1d_fn(TheRange)+Err1,':',color=colFit,
                 label="__no_label")
        plt.plot(TheRange, poly1d_fn(TheRange)-Err1,':',color=colFit,
                 label="__no_label")

    #plt.show(block=False)
    #raw_input("wait")
    LastDate=Dates.iloc[-1]
    plt.title(bf(Country)+": Cannonbal diagram of "+bf(Type)+" including "+LastDate)
    plt.ylim(YLIM)
    plt.xlim(XLIM)
    plt.xlabel("Number of total "+Type)#+" per Million")
    plt.ylabel("Number of new "+Type+" per day")
    plt.legend(loc="best")

    ax=plt.gca()
    ax.spines['left'].set_color(colNewWeek)
    ax.spines['bottom'].set_color(colTotWeek)
    ax.tick_params(axis='y', colors=colNewWeek)
    ax.tick_params(axis='x', colors=colTotWeek)
    ax.yaxis.label.set_color(colNewWeek)
    ax.xaxis.label.set_color(colTotWeek)

    


    ###Left plot
    ##Control to see that projection works
    #plt.plot(Projection2,NewProjection2,":p")
    #plt.plot(Projection2P,NewProjection2P,":p")
    #plt.plot(Projection2M,NewProjection2M,":p")

    plt.plot([],[],"--p",color=colTotDay,label=bf("total")+" "+bf(Type))
    plt.plot([],[],"-p",color=colTotWeek,label=bf("total")+" "+bf(Type)+": average per 7 days")
    lines = plt.gca().get_lines()
    #legend1 = plt.legend([lines[i] for i in [0,1,2]],loc=1)
    #egend2 = plt.legend([lines[i] for i in [3,4,5]],loc=4)
    #plt.gca().add_artist(legend1)
    #plt.gca().add_artist(legend2)
    plt.legend(loc="best")
    
    ###RightHand plot
    ax1 = plt.subplot(1,2,2)
    ax2 = ax1.twinx()
    ax1.spines['left'].set_color(colTotWeek)
    ax1.tick_params(axis='y', colors=colTotWeek)
    ax1.yaxis.label.set_color(colTotWeek)
    ax2.spines['right'].set_color(colNewWeek)
    ax2.tick_params(axis='y',colors=colNewWeek)
    ax2.yaxis.label.set_color(colNewWeek)


    HalfAggLen=AggLen//2
    AggDates=DatesNum[HalfAggLen:-HalfAggLen]
           
    #print "AggDates:",AggDates

    if len(TotCasesDaily) != 0:
        ax1.plot(DatesNum,TotCasesDaily,"--p",color=colTotDay,label=Type+" 7 dat average")

    if len(TotCasesWeekly) != 0:
        ax1.plot(AggDates,TotCasesWeekly,"-p",color=colTotWeek,label=Type+" 7 dat average")

    #plt.plot(DatesForward,FitForward,"--"+cold,label="Projection "+Label)
    #ax2.plot(Dates,NewCasesDaily,"--"+cold+"p",label=Country+" daily")
    if len(NewCasesDaily) != 0:
        ax2.plot(DatesNum,NewCasesDaily,":p",color=colNewDay,label=Type+" 7 day average")
    if len(NewCasesWeekly) != 0:
        ax2.plot(AggDates,NewCasesWeekly,"-p",color=colNewWeek,label=Type+" 7 day average")

    if ShortTerm:
        Y1LIM=give_xlim(TotCasesWeekly[-ShortHistory:],Intercept)
        Y2LIM=give_ylim(NewCasesWeekly[-ShortHistory:])
        SortStr="_short"
    else:
        Y1LIM=give_xlim(TotCasesWeekly,Intercept)
        Y2LIM=give_ylim(NewCasesWeekly)
        SortStr=""

    #ax1.plot(Dates,TotCasesDaily,"--"+cold+"p",label=Country+" daily")


    if ProjectionWorked:
        DatesForward=AggDates[-1] + np.arange(TimeForward) +1
        ax1.plot(DatesForward,TotProjection,"--",color=colTotWeek,label="Projection "+Label)
        ax1.plot(DatesForward,TotProjectionP,":",color=colTotWeek)
        ax1.plot(DatesForward,TotProjectionM,":",color=colTotWeek)
        ax2.plot(DatesForward,NewProjection,"--",color=colNewWeek,label="Projection "+Label)
        ax2.plot(DatesForward,NewProjectionP,":",color=colNewWeek)
        ax2.plot(DatesForward,NewProjectionM,":",color=colNewWeek)

    print("Y2Lim:",Y2LIM)
    if ProjectionWorked:
        Y1ProjLIM=give_ylim(TotProjection,Verbose=False)
        Y2ProjLIM=give_ylim(NewProjection)
        print("Y1ProjLIM:",Y1ProjLIM)
        print("Y2ProjLIM:",Y2ProjLIM)

        Y1LIM=harmonize_limits(Y1LIM,Y1ProjLIM)
        Y2LIM=harmonize_limits(Y2LIM,Y2ProjLIM)


        MaxDate=DatesForward[-1]
    else:
        if len(AggDates) == 0:
            MaxDate = 0
        else:
            MaxDate=AggDates[-1]
    if ShortTerm:
        TickDateRange=np.arange(NumOfDates-ShortHistory,MaxDate+1,7)
    else:
        TickDateRange=np.arange(0,MaxDate+1,DateSplit)
    #print "TickDateRange:", TickDateRange 
    TickDates=[ (datetime.strptime(BaseDate,"%Y-%m-%d") +
                    timedelta(days=int(DF))).strftime("%Y-%m-%d")
                   for DF in TickDateRange]

    #print "TickDates:", TickDates 
    #plt.xticks(TickDateRange,TickDates,rotation="vertical")
    ax1.set_xlim([TickDateRange[0]-.5,TickDateRange[-1]+.5])
    ax1.set_xticks(TickDateRange)
    ax1.set_xticklabels(TickDates,rotation="vertical")

    #ax1.plot(plt.xlim(),[0,0],"-k")
    #ax2.plot(plt.xlim(),[0,0],"-k")
    if Intercept is not None:
        ax1.plot(plt.xlim(),[Intercept,Intercept],"--k")

    ax1.set_ylim(Y1LIM)
    ax2.set_ylim(Y2LIM)

    ax1.set_title("$\\bf{"+Country+"}$: $\\bf{"+Type+"}$ by date up to "+Dates.iloc[-1])
    #plt.ylim([0,plt.ylim()[1]])
    #ax1.legend(loc="best")
    #ax2.legend(loc="best")

    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("countries/"+Country+"_"+Type+SortStr+"_"+LastDate+".png")
    plt.savefig("countries/"+Country+"_"+Type+SortStr+"_"+LastDate+".pdf")
    if not args.q:
        input("Press enter")
    plt.close("all")
    if ProjectionWorked:
        return NewProjection[1]/(1.0*NewProjection[0]),LastDate
    else:
        return np.NaN,LastDate
    


def harmonize_limits(YLIM,YProjLIM):
    ###For downward choose the lowest (rate delines anyway)
    if YLIM[0] < YProjLIM[0]:
        YMin = YLIM[0]
    else:
        ##Choose up to halfway past the current poiny
        YMin = max(YProjLIM[0],YLIM[0] - .5*(YLIM[1] - YLIM[0]))

    if YLIM[1] > YProjLIM[1]:
        YMax = YLIM[1]
    else:
        ##Choose up to halfway past the current poiny
        YMax = min(YProjLIM[1],YLIM[1] + .5*(YLIM[1] - YLIM[0]))
    print("YLimNew:",YMin,YMax)
    return YMin,YMax



def MergeOnDate(TmpLocalData):
    TheDates=np.unique(np.sort(np.array(TmpLocalData['date'],dtype=str)))

    DateNewCases=np.zeros(len(TheDates))
    DateNewHosp=np.zeros(len(TheDates))
    DateNewDeaths=np.zeros(len(TheDates))
    DateNewICU=np.zeros(len(TheDates))
    DateNewTests=np.zeros(len(TheDates))
    for DateNo in range(len(TheDates)):
        print("Date ",DateNo," of ",len(TheDates))
        DateNewCases[DateNo]=np.nansum(TmpLocalData.loc[TmpLocalData["date"]==
                                                TheDates[DateNo],"new_cases"])
        DateNewDeaths[DateNo]=np.nansum(TmpLocalData.loc[TmpLocalData["date"]==
                                                TheDates[DateNo],"new_deaths"])
        DateNewTests[DateNo]=np.nansum(TmpLocalData.loc[TmpLocalData["date"]==
                                                TheDates[DateNo],"new_tests"])
        DateNewICU[DateNo]=np.nansum(TmpLocalData.loc[TmpLocalData["date"]==
                                                TheDates[DateNo],"weekly_icu_admissions"])
        DateNewHosp[DateNo]=np.nansum(TmpLocalData.loc[TmpLocalData["date"]==
                                                TheDates[DateNo],"weekly_hosp_admissions"])



    dataDict = {'date': TheDates,
            'new_cases':  DateNewCases,
            'new_deaths':  DateNewDeaths,
            'new_tests':  DateNewTests,
            'weekly_icu_admissions':  DateNewICU,
            'weekly_hosp_admissions':  DateNewHosp        
            }

    df = pd.DataFrame (dataDict, columns = ['date','new_cases','new_deaths','new_tests','weekly_icu_admissions','weekly_hosp_admissions'])

    return df



import numpy as np
import pandas as pd
import sys
import argparse
from datetime import datetime
from datetime import timedelta
from NumpyHDF5 import *
    
parser = argparse.ArgumentParser(description='Analize the covid data')
parser.add_argument('-q', action="store_true",
                    help='Be quiet == Do not show plots')
parser.add_argument('--short', action="store_true",
                    help='Do only short time')
parser.add_argument('--icu', action="store_true",
                    help='Do only short time')
parser.add_argument('CL', nargs="*", help="Choose countries")

args = parser.parse_args()


if args.q:
    import matplotlib
    matplotlib.use('Agg')
import matplotlib.pyplot as plt 

if args.icu:
    Types = ["hosp"]
else:
    Types = ["cases","deaths","icu","hosp"]

if args.short:
    ShortList=[True]
else:
    ShortList=[True,False]
    
Data = pd.read_table("../2020-covid-19-data/public/data/owid-covid-data.csv",
                     delimiter=",")


###The headers:
##print("The headers: ",Data.columns)

TheContintents=np.array(['Africa', 'Asia', 'Europe', 'North America',
                           'Oceania','South America'],dtype=str)

#args.CL = ["Sweden"]
#args.deaths= True
#args.short= True
#args.q =  True

print("args.CL:",args.CL)
if args.CL==[]:
    CountryList=np.array(np.unique(Data["location"]),dtype=str)
    CountryList=np.append(TheContintents,CountryList)
    #CountryList=np.copy(TheContintents)
else:
    CountryList=args.CL

#Type="cases"
#Type="deaths"
FitHist=14
AggLen=7
ShortHistory=60
DateSplit=20

NumCountries=len(CountryList)
CasesPrognosis = np.zeros(NumCountries)*np.nan
DeathsPrognosis = np.zeros(NumCountries)*np.nan
CountryListS=np.array(CountryList,dtype="S")
DateListS=np.array(CountryList,dtype="|S10")            

for Cno in range(NumCountries):
    Country=CountryList[Cno]
    if np.any(Country == TheContintents):
        TmpLocalData=Data.loc[Data.loc[:,"continent"]==Country]
        LocalData=MergeOnDate(TmpLocalData)
    else:
        LocalData=Data.loc[Data.loc[:,"location"]==Country]
    for Type in Types:
        for ShortTerm in ShortList:
            Prognosis,LastDate=MakeCountryPlot(LocalData,Type,ShortTerm,Country)
            ##-------
            print("Country:",Country)
            print("Type:",Type)
            print("ShortTerm:",ShortTerm)
            if Type == "cases":
                CasesPrognosis[Cno] = Prognosis
            elif Type == "deaths":
                DeathsPrognosis[Cno] = Prognosis
            DateListS[Cno] = LastDate


    SaveManyNumpyAsHDF5([CountryListS,CasesPrognosis,DeathsPrognosis,DateListS],
                        "./Prognosis.hdf5",
                        ["CountryList","CasesPrognosis","DeathsPrognosis","DateListS",])

###Save the prognosis to a separate document

                
if not args.q:
    input("Press enter")

    
